===============================
Taskotron Web Trigger PoC
===============================

PoC for web configured trigger for taskotron

* Free software: GPL2+ license

This code is really rough and is meant as a PoC/demo only. If you're using this
for much of anything else, you may want to rethink that.

Features
--------

* Web accessible triggering mechanism for taskotron - can be used in place of
  or in addition to taskotron-trigger

* Can work as a standalone app by disabling external scheduling mechanism

* No security - anyone can create packages, triggers and evernts
