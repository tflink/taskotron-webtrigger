import os
import webtrigger

if __name__ == '__main__':

    if not (os.getenv('TEST') == 'true' or os.getenv('PROD') == 'true'):
        os.environ['DEV'] = 'true'

    webtrigger.app.run(debug=True)
