#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import codecs
import re

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

here = os.path.abspath(os.path.dirname(__file__))


#class PyTest(Command):
#    user_options = []
#    def initialize_options(self):
#        pass
#    def finalize_options(self):
#        pass
#    def run(self):
#        import subprocess
#        errno = subprocess.call(['py.test', 'testing'])
#        raise SystemExit(errno)


def read(*parts):
    return codecs.open(os.path.join(here, *parts), 'r').read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name='webtrigger',
    version=find_version('webtrigger', '__init__.py'),
    description='PoC for web configured trigger for taskotron',
    author='tflink',
    author_email='tflink@fedoraproject.org',
    url='https://bitbucket.org/tflink/webtrigger',
    packages=[
        'webtrigger', 'webtrigger.controllers', 'webtrigger.models', 'webtrigger.lib'
    ],
    package_dir={'webtrigger': 'webtrigger'},
    include_package_data=True,
    license="GPL2+",
    keywords='webtrigger',
    entry_points=dict(console_scripts=['webtrigger=webtrigger.cli:main']),
#    test_suite='tests',
)

