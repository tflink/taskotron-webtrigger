# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>

from flask import Blueprint, render_template
from sqlalchemy import desc

from webtrigger import app, db
from webtrigger.controllers import forms
from webtrigger.models import Package, Event, Trigger, Job
from webtrigger.lib.jobschedule import schedule_job


main = Blueprint('main', __name__)

# this isn't the most elegant way to keep the item_types consistent but it
# works for a crummy PoC
item_types = [('koji_build','koji_build'), ('koji_tag','koji_tag'), ('compose','compose')]

@main.route('/')
def index():
    if app.debug:
        app.logger.debug('rendering index')
    jobs = Job.query.all()
    return render_template('index.html', title='Webtrigger Index', jobs=jobs)


@main.route('/package', methods = ["GET", "POST"])
def package():
    pkg_form = forms.PackageForm()

    if pkg_form.validate_on_submit():
        pkg_name = pkg_form.name.data

        app.logger.info("adding package {}".format(pkg_name))
        pkg = Package(pkg_name)
        db.session.add(pkg)
        db.session.commit()

    known_packages = Package.query.all()

    return render_template('package.html', title='Packages', known_packages=known_packages, pkg_form=pkg_form)


@main.route('/trigger', methods = ["GET", "POST"])
def trigger():
    trigger_form = forms.TriggerForm()
    trigger_form.item_type.choices = [('koji_build','koji_build'), ('koji_tag','koji_tag'), ('compose','compose')]
    trigger_form.package.choices = [(p.id, p.name) for p in Package.query.order_by('name').all()]

    if trigger_form.validate_on_submit():
        task_name = trigger_form.task_name.data
        item_type = trigger_form.item_type.data
        package_id = trigger_form.package.data
        repourl = trigger_form.repourl.data
        print("got trigger request for {} ({}), {}, {}".format(task_name, repourl, item_type, package_id))

        # look for package
        package = Package.query.filter_by(id=package_id).first()

        trigger = Trigger(package, item_type, task_name, repourl)

        db.session.add(trigger)
        db.session.commit()

    triggers = Trigger.query.all()
    return render_template('trigger.html', title='Task Triggers', triggers=triggers, trigger_form=trigger_form)


@main.route('/event', methods=['GET', 'POST'])
def event():
    event_form = forms.EventForm()
    event_form.item_type.choices = [('koji_build','koji_build'), ('koji_tag','koji_tag'), ('compose','compose')]
    event_form.package.choices = [(p.id, p.name) for p in Package.query.order_by('name').all()]

    if event_form.validate_on_submit():
        item = event_form.item.data
        item_type = event_form.item_type.data
        package_id = event_form.package.data
        print("got trigger request for {}, {}, {}".format(item, item_type, package_id))

        # look for package
        package = Package.query.filter_by(id=package_id).first()

        event = Event(package, item, item_type)
        db.session.add(event)

        # look for triggers so we can schedule jobs
        triggers = Trigger.query.filter_by(package=package, item_type=item_type).all()

        for trigger in triggers:
            app.logger.info("Scheduling {}({}) for {} ({}) from package {}".format(trigger.task_name, trigger.repourl, item, item_type, package.name))
            newjob = Job(event, trigger)
            db.session.add(newjob)
            if app.config['SCHEDULE']:
                schedule_job(item, item_type, trigger.task_name, trigger.repourl, 'x86_64', use_disposable=True)

        db.session.commit()

        # now that we've got jobs in the db, let's try scheduling them

    elif len(event_form.errors) > 0:
        print event_form.item.errors
        print event_form.item_type.errors
        print event_form.package.errors
        for error in event_form.errors:
            print error

    recent_events = Event.query.order_by(desc(Event.timestamp)).limit(10)

    return render_template('event.html', title='list', event_form=event_form, recent_events=recent_events)
