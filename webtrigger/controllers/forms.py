# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>

from flask.ext.wtf import Form
from wtforms import TextField, SelectField
from wtforms.validators import Required

class TriggerForm(Form):
    task_name = TextField('Task Name', [Required()])
    item_type = SelectField('item_type', [Required()])
    package = SelectField('package', [Required()], coerce=int)
    repourl = TextField('Task Repo URL', [Required()])

class EventForm(Form):
    item = TextField('Item', [Required()])
    item_type = SelectField('item_type', [Required()])
    package = SelectField('package', [Required()], coerce=int)

class PackageForm(Form):
    name = TextField('Package Name', [Required()])
