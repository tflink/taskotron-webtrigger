# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>

from datetime import datetime
import uuid as uuid_pkg

from webtrigger import db

class Package(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(2048))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<package {}>'.format(self.name)

class Trigger(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task_name = db.Column(db.String(2048))
    repourl = db.Column(db.String(2048))
    item_type = db.Column(db.String(2048))
    package_id = db.Column(db.Integer, db.ForeignKey('package.id'))
    package = db.relationship('Package', foreign_keys=[package_id], backref='triggers')

    def __init__(self, package, item_type, task_name, repourl):
        self.package = package
        self.task_name = task_name
        self.item_type = item_type
        self.repourl = repourl

    def __repr__(self):
        return '<trigger {},{},{}({})>'.format(self.package.name, self.item_type, self.task_name, self.repourl)

class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime)
    package_id = db.Column(db.Integer, db.ForeignKey('package.id'))
    package = db.relationship('Package', foreign_keys=[package_id], backref='events')
    item = db.Column(db.String(2048))
    item_type = db.Column(db.String(2048))

    def __init__(self, package, item, item_type):
        self.package = package
        self.item = item
        self.item_type = item_type
        self.timestamp = datetime.utcnow()

    def __repr__(self):
        return '<event {},{} ({}),{}>'.format(self.package.name, self.item, self.item_type, self.timestamp.strftime('%Y%m%d-%H:%M:%S'))

class Job(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime)
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))
    event = db.relationship('Event', foreign_keys=[event_id], backref='jobs')
    trigger_id = db.Column(db.Integer, db.ForeignKey('trigger.id'))
    trigger = db.relationship('Trigger', foreign_keys=[trigger_id], backref='jobs')
    uuid = db.Column(db.String(36), unique=True)

    def __init__(self, event, trigger, uuid=None):
        self.event = event
        self.trigger = trigger
        self.timestamp = datetime.utcnow()
        if uuid is None:
            self.uuid = str(uuid_pkg.uuid1())
        else:
            self.uuid = uuid

    def __repr__(self):
        return '<job {},{},{} ({})>'.format(self.timestamp.strftime('%Y%m%d-%H:%M:%S'), self.trigger.task_name, self.event.item, self.event.item_type)

