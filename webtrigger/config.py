# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>

class Config(object):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    LOGFILE = '/var/log/blockerbugs/blockerbugs.log'
    FILE_LOGGING = False
    SYSLOG_LOGGING = False
    STREAM_LOGGING = True
    PRODUCTION = False
    FEDMENU_URL = ""
    FEDMENU_DATA_URL = ""


class ProductionConfig(Config):
    DEBUG = False
    FAS_HTTPS_REQUIRED = True
    PRODUCTION = True


class DevelopmentConfig(Config):
    TRAP_BAD_REQUEST_ERRORS = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/webtrigger_db.sqlite'


class TestingConfig(Config):
    TESTING = True

