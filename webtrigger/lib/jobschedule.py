# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>

import requests
import json

from webtrigger import app

# shamelessly stolen from taskotron-trigger

def schedule_job(item, item_type, taskname, repourl, arch='x86_64', use_disposable=True):
    output = ["Sending to Buildmaster:",
                "item: %s" % item,
                "item_type: %s" % item_type,
                "taskname: %s" % taskname,
                "arch: %s" % arch]

    app.logger.debug('\n'.join(output))

    properties = {
                'item': item,
                'item_type': item_type,
                'taskname': taskname,
                'arch': arch,
                'repourl': repourl
                }

    category = 'disposable' if use_disposable else arch

    try:
        # Create new job in ExecDB
        req_uuid = requests.post("%s/jobs" % app.config['EXECDB_URL'],
                            headers={'Content-type': 'application/json',
                                    'Accept': 'text/plain'},
                            data=json.dumps(properties))

        if req_uuid.status_code != 201:
            raise Exception("ExecDB - bad status code: %s" % req_uuid.status_code)
        else:
            uuid = req_uuid.json()['uuid']

    except requests.exceptions.ConnectionError:
        app.logger.error("error: connection to ExecDB failed; %r" % properties)

    else:
        properties['uuid'] = uuid

        r = requests.post(app.config['BUILDMASTER_URL'],
                    data={'author': 'taskotron',
                            'project': 'rpmcheck',
                            'category': category,
                            'repository': '',
                            'comments': 'build request from taskotron-trigger',
                            'properties': json.dumps(properties)})

        app.logger.info("post-hook build request status: %s" % str(r.status_code))
        return uuid

