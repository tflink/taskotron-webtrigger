# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>

# mostly borrowed from resultsdb

import sys
import os
from optparse import OptionParser

from alembic.config import Config
from alembic import command as al_command
from alembic.migration import MigrationContext
from sqlalchemy.engine import reflection

from webtrigger import db
from webtrigger import models

def get_alembic_config():
    # the location of the alembic ini file and alembic scripts changes when
    # installed via package
    if os.path.exists("./alembic.ini"):
        alembic_cfg = Config("./alembic.ini")
    else:
        alembic_cfg = Config("/usr/share/resultsdb/alembic.ini",
                             ini_section='alembic-packaged')
    return alembic_cfg

def upgrade_db(*args):
    print "Upgrading Database to Latest Revision"
    alembic_cfg = get_alembic_config()
    al_command.upgrade(alembic_cfg, "head")

def init_alembic(*args):
    alembic_cfg = get_alembic_config()

    # check to see if the db has already been initialized by checking for an
    # alembic revision
    context = MigrationContext.configure(db.engine.connect())
    current_rev = context.get_current_revision()

    if not current_rev:
        print "Initializing alembic"
        print " - Setting the current version to the first revision"
        al_command.stamp(alembic_cfg, "e7985d2bd86")
    else:
        print "Alembic already initialized"

def initialize_db(destructive):
    alembic_cfg = get_alembic_config()

    print "Initializing database"

    if destructive:
        print " - Dropping all tables"
        db.drop_all()

    # check whether the table 'job' exists
    # if it does, we assume that the database is empty
    insp = reflection.Inspector.from_engine(db.engine)
    table_names = insp.get_table_names()
    if 'job' not in table_names and 'Job' not in table_names:
        print " - Creating tables"
        db.create_all()
        print " - Stamping alembic's current version to 'head'"
        al_command.stamp(alembic_cfg, "head")

    # check to see if the db has already been initialized by checking for an
    # alembic revision
    context = MigrationContext.configure(db.engine.connect())
    current_rev = context.get_current_revision()
    if current_rev:
        print " - Database is currently at rev %s" % current_rev
        upgrade_db(destructive)
    else:
        print "WARN: You need to have your db stamped with an alembic revision"
        print "      Run 'init_alembic' sub-command first."

#def initialize_db(destructive):
#    print "Initializing database"
#
#    if destructive:
#        print " - Dropping all tables"
#        db.drop_all()
#
#    db.create_all()

def mock_data(destructive):
    print "Populating tables with mock-data"

    if destructive or not db.session.query(models.Package).count():
        pkg1 = models.Package('libtaskotron')
        pkg2 = models.Package('resultsdb')
        pkg3 = models.Package('blockerbugs')

        trg1 = models.Trigger(pkg1, 'koji_build', 'libtaskotron-distgit', 'git@127.0.0.1:task-git/libtaskotron.git')
        trg2 = models.Trigger(pkg2, 'koji_build', 'resultsdb-distgit', 'git@127.0.0.1:task-git/resultsdb.git')
        trg3 = models.Trigger(pkg2, 'koji_build', 'blockerbugs-distgit', 'git@127.0.0.1:task-git/blockerbugs.git')


        db.session.add(pkg1)
        db.session.add(pkg2)
        db.session.add(pkg3)
        db.session.add(trg1)
        db.session.add(trg2)
        db.session.add(trg3)

        db.session.commit()
    else:
        print " - skipped addingpackages"


def main():
    possible_commands = ['init_db', 'mock_data', 'upgrade_db', 'init_alembic']

    usage = 'usage: [DEV=true] %prog ' + "(%s)" % ' | '.join(possible_commands)
    parser = OptionParser(usage=usage)
    parser.add_option("-d", "--destructive",
                  action="store_true", dest="destructive", default=False,
                  help="Drop tables in `init_db`; Store data in `mock_data` "
                  "even if the tables are not empty")

    (options, args) = parser.parse_args()

    if len(args) != 1 or args[0] not in possible_commands:
        print usage
        print
        print 'Please use one of the following commands: %s' % str(possible_commands)
        sys.exit(1)

    command = {
                'init_db': initialize_db,
                'mock_data': mock_data,
                'upgrade_db': upgrade_db,
                'init_alembic': init_alembic,
              }[args[0]]

    if not options.destructive:
        print "Proceeding in non-destructive mode. To perform destructive "\
              "steps use -d option."

    command(options.destructive)

    sys.exit(0)


if __name__ == '__main__':
    main()

