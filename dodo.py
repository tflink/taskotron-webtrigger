#! /usr/bin/env python
# encoding: utf-8

import os
import codecs
import re
import subprocess
from datetime import datetime

from doit.tools import create_folder
from doit import get_var


################################################################################
# PROJECT DEFINITIONS
# This dodo file is designed to be pretty much identical in all qadevel
# projects. This section should be the only part which differs between them.
################################################################################

NAME = 'webtrigger'
MOCKENV = 'fedora-21-x86_64'
TARGETDIST = 'fc21'
BUILDARCH = 'noarch'
COPRREPO = 'https://copr-be.cloud.fedoraproject.org/results/tflink/taskotron/fedora-21-x86_64/'


################################################################################
# UTILITY FUNCTIONS
# Functions used to generate some of the needed metadata
################################################################################

def get_version():
    """ Assume that the version is stored as __version__ in $APPNAME/__init__.py
    so that it can be generically extracted
    """

    version_file = codecs.open(os.path.join(NAME, '__init__.py'), 'r').read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


def get_rpmrelease(specfile):
    """parse the rpm release out of the specfile that we're currently using"""
    rpmspec_command = ['rpmspec', '-q', '--queryformat="%{RELEASE}\\n"', specfile]
    raw_output = subprocess.check_output(rpmspec_command).split()[0].lstrip('"')
    return raw_output.split('.')[0]


################################################################################
# GENERAL VARIABLES
# These variables are computed based on project definitions and current state
# as a convenience for later functions
################################################################################

VERSION = get_version()
TARGETNAME = "{}-{}".format(NAME, VERSION)
SPECFILE = '{}.spec'.format(NAME)
RPMRELEASE = get_rpmrelease(os.path.abspath(SPECFILE))
NVR = '{}-{}-{}.{}'.format(NAME, VERSION, RPMRELEASE, TARGETDIST)
SHATYPE = 'sha256'

HERE = os.path.abspath(os.path.dirname(__file__))
OUTDIR = os.path.join(HERE, 'builds/{}'.format(VERSION))

# these are temporary, should be options with defaults
BRANCH = get_var('branch', 'develop')


################################################################################
# PROJECT TASKS
# These tasks should be pretty much consistent for all qadevel projects, driven
# by the variables listed above
################################################################################

def task_chainbuild():
    """mockchain package using COPR repo to assist. Will fail if deps in repo are not up to date"""
    tmpdir_prefix = '{}-{}'.format(NAME, datetime.utcnow().strftime('%Y%m%d%H%M%S'))
    mockchain_command = 'mockchain -r {} --tmp_prefix={} -a {} '\
                        '{}/{}.src.rpm'.format(MOCKENV, tmpdir_prefix,
                                               COPRREPO, OUTDIR, NVR)
    cp_command = 'cp /var/tmp/mock-chain-{}*/results/{}/{}/{}.{}.rpm '\
                 '{}/.'.format(tmpdir_prefix, MOCKENV, NVR, NVR, BUILDARCH,
                               OUTDIR)

    return {'actions': [mockchain_command, cp_command],
            'task_dep': ['buildsrpm'],
            'targets': ['{}.rpm'.format(NVR)]
            }


def task_buildsrpm():
    """build SRPM using mock, the latest git shapshot and current specfile details"""
    mocksrpm_command = 'mock -r {} --buildsrpm --spec={} '\
                       '--sources={}'.format(MOCKENV, SPECFILE, OUTDIR)
    cp_command = 'cp /var/lib/mock/{}/result/{}-{}-{}.{}.src.rpm '\
                 '{}/.'.format(MOCKENV, NAME, VERSION, RPMRELEASE, TARGETDIST,
                               OUTDIR)

    return {'actions': [mocksrpm_command, cp_command],
            'task_dep': ['snapshot'],
            'targets': ['{}.src.rpm'.format(NVR)]
            }


def task_snapshot():
    """Take snapshot of git specified git branch"""
    archive_command = 'git archive {} --prefix={}/ | gzip -c9 > '\
                      '{}.tar.gz'.format(BRANCH,
                                         TARGETNAME,
                                         os.path.join(OUTDIR, TARGETNAME))

    return {'actions': [(create_folder, [OUTDIR]),
                        archive_command],
            'targets': ['{}.tar.gz'.format(TARGETNAME)],
            }


def task__checksum():
    """Generate checksum of git snapshot"""
    sha_command = '{shatype}sum {outdir}/{target}.tar.gz > '\
                  '{outdir}/{target}.tar.gz.{shatype}'.format(shatype=SHATYPE,
                                                              target=TARGETNAME,
                                                              outdir=OUTDIR)
    return {'actions': [sha_command],
            'task_dep': ['snapshot'],
            'targets': ['{}.tar.gz.{}'.format(TARGETNAME, SHATYPE)]
            }


def task_builddocs():
    """Generate sphinx html docs"""
    return {'actions': ['pushd docs;make html'],
            'targets': ['docs/build/html']
            }


def task_cleandocs():
    """Clean generated sphinx html docs"""
    return {'actions': ['pushd docs;make clean'],
            }


def task_releasedocs():
    """Put generated docs in the release output"""
    return {'actions': ['cp -r docs/build/html {}/docs'.format(OUTDIR)],
            'task_dep': ['builddocs'],
            'targets': ['{}/docs'.format(OUTDIR)]
            }


def task_test():
    """Run the unit and functional tests"""
    return {'actions': ['py.test -F testing/']
            }


def task__details():
    """ Display some calculated variables used in the script for debug purposes"""

    return {'actions': ['echo "taskotron version: {}"'.format(VERSION),
                        'echo "target name: {}"'.format(TARGETNAME),
                        'echo "output dir: {}"'.format(OUTDIR)],
            'verbosity': 2
            }
