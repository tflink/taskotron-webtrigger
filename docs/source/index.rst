================================================
Taskotron Webtrigger PoC
================================================

This project is a rough PoC meant to both demo the capabilities of Taskotron's
disposable clients and as an example of what we can do with a web interface
to our current trigger system.

Nothing here should be taken as decided - it's just a PoC :)

This is a pretty ad-hoc set of docs, meant as much to record what is different
from current production taskotron/trigger as it is to be documentation.

Changes
=======

There are several areas of changes that I made changes to in order to get the
demo working

Properties Passed to Buildmaster
--------------------------------

* category: 'disposable' is needed to not be routed to the default
  non-disposable builder

* taskname: in the past, we've assumed that the repository containing the task
  we want to execute is always going to be based on task name and the filename
  of the formula to execute is ``<taskname>.yml``. To make things a bit more
  flexible, I've changed the following

    - added ``repourl`` to the properties sent to the buildmaster on trigger

    - still assuming that the task file is ``<taskname>.yml`` inside that repo

    - I still want to have a convention and sane defaults like looking for a
      file named ``taskotron.yml`` in the root of the repo as the default formula
      or maybe even some sort of ini file describing the checks present in the
      repo. The details are something to be figured out later but I needed it
      now to get things working


Buildmaster Configuration
-------------------------

There have been several changes, can be seen in the ansible repo

Buildslave Configuration and Deployment
---------------------------------------

TBD for more than 1 client

For now, the setup will only work for one buildslave per host. Haven't quite
figured out how we're going to work around this limitation but that doesn't
have to be dealt with right now.


(Potential) Issues
==================

Ran into a small issue with testcloud where the buildslave user couldn't delete
the instance dir when there were issues during execution where the buildbot
job hung. Once I got things working, this issue seems to have gone away and
I'm not quite sure what was going wrong.

I had to explicitly set /home/buildslave/.ssh/id_rsa as the ssh_privkey in
taskotron config.



.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

