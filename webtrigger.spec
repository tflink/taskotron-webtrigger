# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

Name:           webtrigger
Version:        0.0.4
Release:        1%{?dist}
Summary:        PoC for web configured trigger for taskotron

License:        GPL2+
URL:            https://bitbucket.org/tflink/taskotron-webtrigger
Source0:        %{name}-%{version}.tar.gz

BuildArch:      noarch

Requires:       python-flask
Requires:       python-flask-sqlalchemy
Requires:       python-flask-wtf
Requires:       python-requests
Requires:       python-alembic
BuildRequires:  python-devel
BuildRequires:  python-setuptools
BuildRequires:  pytest
BuildRequires:  python-flask
BuildRequires:  python-flask-sqlalchemy
BuildRequires:  python-fedora-flask
BuildRequires:  python-mock
BuildRequires:  python-requests

%description
PoC for web configured trigger for taskotron needs a better description

%prep
%setup -q

#%check
#%{__python} setup.py test


%build
%{__python} setup.py build

# no docs yet
# sphinx-build  -b html -d docs/_build/doctrees docs/source docs/_build/html
# sphinx-build  -b man -d docs/_build/doctrees docs/source docs/_build/man

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

# alembic stuff
mkdir -p %{buildroot}%{_datadir}/webtrigger
cp -r alembic %{buildroot}%{_datadir}/webtrigger/.
cp alembic.ini %{buildroot}%{_datadir}/webtrigger/.

# apache config and wsgi stuff
mkdir -p %{buildroot}%{_datadir}/webtrigger/conf
cp conf/webtrigger.conf %{buildroot}%{_datadir}/webtrigger/conf/.
cp conf/webtrigger.wsgi %{buildroot}%{_datadir}/webtrigger/.

# configuration file
mkdir -p %{buildroot}%{_sysconfdir}/webtrigger
install conf/settings.py.example %{buildroot}%{_sysconfdir}/webtrigger/settings.py.example

# man page for CLI
#mkdir -p %{buildroot}/%{_mandir}/man1/
#cp -v docs/_build/man/*.1 %{buildroot}/%{_mandir}/man1/


%files
#%doc docs/_build/html/*
#%doc %{_mandir}/man1/webtrigger*

%{python_sitelib}/*

%attr(755,root,root) %{_bindir}/webtrigger
%dir %{_sysconfdir}/webtrigger
%{_sysconfdir}/webtrigger/*
%dir %{_datadir}/webtrigger
%{_datadir}/webtrigger/*


%changelog
* Mon Aug 10 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.4-1
- un-botching copypasta error with alembic

* Mon Aug 10 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.3-1
- Bumping verison and adding alembic bits, deps

* Fri Aug 7 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.2-1
- Initial package for Taskotron Web Trigger

* Thu Aug 6 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.1-1
- Initial package for Taskotron Web Trigger
